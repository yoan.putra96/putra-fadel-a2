/* Ini adalah file main.js yang akan menjadi tempat menulis javascript */

$(document).ready(function () {
  $(".owl-carousel").owlCarousel({
    items: 1,
    nav: true,
    dots: true,
    navText: [
      "<span><i class='fa-solid fa-arrow-left'></i></span>",
      "<span><i class='fa-solid fa-arrow-right'></i></span>",
    ],
    responsive: {
      1023: {
        items: 3,
      },
    },
  });
});

//=========================== Initializing AOS ==================================//
AOS.init({
  delay: 400,
  duration: 1000,
});
